.. index:: Geometry Nodes; Displace Hair Curves

********************
Displace Hair Curves
********************

Displaces hair curves by a vector based on various options.

.. peertube:: 8DP1hsJucDanf2s54P1qe3


Inputs
======

Geometry
   Input Geometry (only curves will be affected).

Factor
   Factor to scale overall displacement.

Shape
   Shape of the influence along curves (0=constant, 0.5=linear).

Object Space
   Object used to define the displacement space.

Displace Vector
   Vector for displacement.

Surface
   Surface geometry used to sample the normal for displacement. This input takes priority over the corresponding
   object input, if used.

Surface
   Surface object used to sample the normal for displacement.

Surface UV Map
   Surface UV map used to sample the normal for displacement.

Surface Normal Displacement
   Amount of displacement along the surface normal.


Properties
==========

This node has no properties.


Outputs
=======

**Geometry**
